��          �            x     y     �     �     �     �     �  
   �     �          *     D     U     ^     r     �  �  �  9     7   R  9   �  (   �  &   �  u        �  *   �  9   �  ?        O     o  =   �     �  E   �                                         
                           	           animated weather icons invalid owm key not animated weather icons owm api key example owm api key input owm api key link show today temperature in Kelvin temperature in celsius temperature in fahrenheit temperature unit validate weather_description weather_title which city? Project-Id-Version: mirr.OS v0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-03-17 10:20+0100
PO-Revision-Date: 2018-08-16 10:15+0800
Last-Translator: gorbo <info@gorbo.de>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.8.6
Language: en_GB
 Значки с анимированной погодой Недействительный OpenWeatherMap Api-Key Неанимированные значки погоды. (e.g.: c863d93d1c7ff4ef9bace44847e40e98) Введите OpenWeatherMap Api-Key. Здесь вы получаете <a href="http://openweathermap.org/appid" target="_blank">openweather Api-Key</a>. показать сегодня температура в Кельвине температуры в градусах Цельсия температуры в градусах Фаренгейта блок температуры подтвердить Прогноз погоды на следующие 4 дня. Погода В каком городе вы хотите знать погоду? 